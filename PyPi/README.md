# How to Setup Automated Testing and publish a PyPi Package in 2023

In diesem Abschnitt erhalten Sie eine Einführung zur Veröffentlichung von Python packages auf PyPi. Die bereitgestellte Selbstlerneinheit beinhaltet dabei folgende Punkte:

- Motivation
- Walkthrough : creating a package and setting up testing
- Build and Upload to PyPi 
- Project Setup Shortcut

### Nachnutzung
Bei einer Nachnutzung von diesen Unterlagen wählen Sie bitte eine der untenstehenden Möglichkeiten aus.

<details><summary><ins>Die Materialien wurden nicht verändert</ins></summary>

Um Materialien von diesem GitLab nachzunutzen, empfehlen wir Ihnen für eine vollständige Namensnennung folgendes:
> <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a> Dieser Inhalt wurde von dem [NFDI4Ing Education GitLab](https://git.rwth-aachen.de/nfdi4ing/education) nachgenutzt unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Namensnennung 4.0 International Lizenz</a>. Die nachgenutzten Materialien finden sich unter: https://git.rwth-aachen.de/nfdi4ing/education/software-development/pypi

Sollten es nicht möglich sein, die vollständige Namensnennung aufzuführen, nutzen Sie bitte folgenden Link:
> https://git.rwth-aachen.de/nfdi4ing/education/software-development/-/blob/main/PyPi/LICENSE_Unver%C3%A4ndert

</details>

<details><summary><ins>Die Materialien wurden verändert</ins></summary>

Um Materialien von diesem GitLab nachzunutzen, empfehlen wir Ihnen für eine vollständige Namensnennung folgendes:
> <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a> Dieser Inhalt wurde von dem [NFDI4Ing Education GitLab](https://git.rwth-aachen.de/nfdi4ing/education) nachgenutzt und verändert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Namensnennung 4.0 International Lizenz</a>. Die nachgenutzten Materialien finden sich unter: https://git.rwth-aachen.de/nfdi4ing/education/software-development/pypi

Sollten es nicht möglich sein, die vollständige Namensnennung aufzuführen, nutzen Sie bitte folgenden Link:
> https://git.rwth-aachen.de/nfdi4ing/education/software-development/-/blob/main/PyPi/LICENSE_Ver%C3%A4ndert

</details>

Weitere Infos zu Lizenzen und der Nachnutzung der hier bereitgestellten Materialien finden Sie [hier](https://git.rwth-aachen.de/groups/nfdi4ing/education/-/wikis/Lizenzen).

### Lizenz
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a> Diese Seite und ihre Inhalte sind lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons Namensnennung 4.0 International Lizenz</a>.
